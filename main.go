package main

import (
	"gitlab.com/king011/revel-i18n/cmd"
	"log"
)

func main() {
	// 執行命令
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
